# vim: set filetype=bash
export PATH=$PATH:/home/nero/.bin
export PATH=$PATH:/home/nero/.yarn/bin
export PATH=$PATH:/home/nero/.cargo/bin
export PATH=$PATH:/home/nero/.poetry/bin
export PATH=$PATH:/var/lib/flatpak/exports/bin
export PATH=$PATH:/home/nero/.local/bin # pipenv & stuff

# add pyenv to path
export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

alias clc=python3
alias tocbr='for folder in */; do rar a -m0 -r "${folder%/}.cbr" "$folder"; done'
alias chckf='checkfiles.py'

# rotate pdfs
rot_left () { pdftk $1 cat 1-endwest output tmp.pdf && mv tmp.pdf $1;}
rot_top () { pdftk $1 cat 1-endnorth output tmp.pdf && mv tmp.pdf $1;}
rot_right () { pdftk $1 cat 1-endeast output tmp.pdf && mv tmp.pdf $1;}
rot_bot () { pdftk $1 cat 1-endsouth output tmp.pdf && mv tmp.pdf $1;}

# go up directory shortcuts
alias up="cd .."
alias ..2="cd ../.."
alias ..3="cd ../../.."
alias ..4="cd ../../../.."

# unlimited bash history
HISTSIZE=-1
HISTFILESIZE=-1

# python .venv in project root
export PIPENV_VENV_IN_PROJECT=1
export POETRY_VIRTUALENVS_IN_PROJECT=1

# human readable file sizes with ls-l
alias lsl='ls -lh'

# fuzzy find binding to CTRL+R
source /usr/share/doc/fzf/examples/key-bindings.bash

# sudo previous command
alias please='sudo "$BASH" -c "$(history -p !!)"'

# open files with default application, e.g. .zip
alias open='xdg-open'

explain () {
	history | tail -n 2 | head -n 1 | sed 's/ [0-9]*  //' | sed 's/ /+/g' | awk '{print "https://explainshell.com/explain?cmd="$1}' | xargs xdg-open;
}

# rsync shows progress
alias cp2="rsync -ah --progress"

# make sound after long running command
alias notify="aplay -q ~/.sounds/notify.wav"

# copy other pub key to authorized keys of server
# $1 = ssh pub key file, e.g. ~/.ssh/id_rsa_wj.pub
# $2 = ssh acc and host, e.g. ubuntu@wj.getmarple.io
ssh-copy-other-key () {
  cat $1 | ssh $2 'cat >> .ssh/authorized_keys && echo "Key copied"'
}


# copy ssh key to AWS EC2
# assuming aws.pem in ~/.ssh
# $1 and $2 same as ssh-copy-other-key above
aws-copy-ssh-key () {
  cat $1 | ssh -i /home/nero/.ssh/aws.pem $2 'cat >> .ssh/authorized_keys && echo "Key copied"'
}

# start a socks ssh proxy
alias putonsocks="ssh -f -N -M -S /tmp/sshtunnel -D 1080 ubuntu@hop.marpledata.com -p22"

# see most recent files
alias recent="find $1 -type f -exec stat --format '%Y :%y %n' \"{}\" \; | sort -nr | cut -d: -f2- | head -n 20"

# get public ip
alias myip="curl ifconfig.me; echo"

# cat but with syntax highlighting, requires pip install pygmentize
alias dog="pygmentize -g -O style=material"

# go to marple repos
alias mco="cd ~/git/marple-core"
alias mui="cd ~/git/marple-ui"
alias mdo="cd ~/git/marple-deployment"

# typos
alias gti=git

# highlight a string in text
highlight() {
  grep --color -iE "$1|\$"
}

# open gitlab repo in the browser
alias gitlab="glab repo view -w"

# fd
alias fd="fdfind"

# autojump
. /usr/share/autojump/autojump.sh

# shark
export SHARK_CONFIG=/home/nero/git/linux-config/shark.json

# flatpak shorter names
alias code="com.visualstudio.code"
alias meld="org.gnome.meld"

# boot logs
alias boots="journalctl --list-boots"
alias boot-logs="journalctl -b -1 -e" # or -2, -3, ...

# experiment with a docker container in bash
# $1 = image name, e.g. python:3.10-buster
test-container () {
  docker run -it $1 /bin/bash
}


# mindpal
export SECOND_MIND_CONFIG=/home/nero/git/linux-config/mind-palace
mindpal() {
  history | tail -n 2 | head -n 1 | mindpal-lookup
}
alias 2mind=mindpal


# llmhelp
llmhelp() {
  last_command=$(history | tail -n 2 | head -n 1 | sed 's/^[[:space:]]*[0-9]*[[:space:]]*//')
  output=$(bash -c "$last_command" 2>&1)
  prompt="I executed this bash command: $last_command\n The output was: $output but has an error. Suggest a new command to fix it."
  # requires llm and llm-cmd: https://github.com/simonw/llm-cmd
  llm cmd $prompt
}