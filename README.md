# Dotfiles

### Installation

`./install.sh` does most of the work

Pycharm keymap

- Needs to be a `.zip` with all IDE settings, including the keymap
- Import with `File` $\rightarrow$ `Manage IDE Settings` $\rightarrow$ `Import Settings`

### Custom keyboard shortcuts

**screenshot-area**

- `Shift`+`Print`
- `gnome-screenshot -a`

**screenshot-area-copy**

- `Shift`+`Ctrl`+`Print`
- `bash -c "gnome-screenshot -a -f /tmp/screenshot.png && xclip -in -selection clipboard -target image/png /tmp/screenshot.png"`

**screenshot-area-copy-text**

- `Ctrl`+`Alt`+`Print`
- `bash -c "gnome-screenshot -a -f /tmp/screenshot.png && tesseract /tmp/screenshot.png stdout | xclip -sel clip"`

**screenshot-screen**

- `Print`
- `gnome-screenshot`

**screenshot-screen-copy**

- `Ctrl`+`Print`
- `bash -c "gnome-screenshot -f /tmp/screenshot.png && xclip -in -selection clipboard -target image/png /tmp/screenshot.png"`

**unlauncher**

- `Super`+`Space`
- `ulauncher-toggle`
