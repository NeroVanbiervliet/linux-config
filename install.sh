#!/usr/bin/env bash
cp -f bash_aliases $HOME/.bash_aliases
cp -f bash_profile $HOME/.bash_profile
cp -f gitconfig $HOME/.gitconfig
cp -rp githooks $HOME/.githooks
mkdir -p $HOME/.sounds
cp notify.wav $HOME/.sounds
mkdir -p $HOME/.bin
cp -rf bin/* $HOME/.bin/
cp ssh/* $HOME/.ssh

# vscode
VSCODE_PATH=$HOME/.var/app/com.visualstudio.code/config/Code/User
cp keymaps/vscode/* $VSCODE_PATH

# put screenshots in ~/sync/screenshots
gsettings set org.gnome.gnome-screenshot auto-save-directory "file:///home/$USER/sync/screenshots"

# switch with Ctrl+(Shift)+Tab in gnome-terminal
# from https://askubuntu.com/questions/133384/keyboard-shortcut-gnome-terminal-ctrl-tab-and-ctrl-shift-tab-in-12-04
gsettings set org.gnome.Terminal.Legacy.Keybindings:/org/gnome/terminal/legacy/keybindings/ next-tab '<Primary>Tab'
gsettings set org.gnome.Terminal.Legacy.Keybindings:/org/gnome/terminal/legacy/keybindings/ prev-tab '<Primary><Shift>Tab'

# unlimited bash history
sed -i 's/HISTSIZE=1000/HISTSIZE=/g' ~/.bashrc
sed -i 's/HISTFILESIZE=2000/HISTFILESIZE=/g' ~/.bashrc
