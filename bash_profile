# vim: set filetype=bash
# delete files older than 2 days in ~/Downloads
find /home/nero/Downloads -not -path "/home/nero/Downloads/_safe/*" \( -type f -o -type l \) -mtime +2 -delete -print >> /home/nero/Downloads/_safe/delete-log
find /home/nero/Downloads -not -path "/home/nero/Downloads/_safe/*" -type d -empty -delete -print >> /home/nero/Downloads/_safe/delete-log

# set new wallpaper
~/.bin/shuffle-wallpaper
